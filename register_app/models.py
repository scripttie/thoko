from django.db import models


class Farmer(models.Model):
	mobileNumber = models.CharField(max_length=255, unique=True)
	name = models.CharField(max_length=255)
	location = models.CharField(max_length=255)


class Buyer(models.Model):
	mobileNumber = models.CharField(max_length=255, unique=True)
	name = models.CharField(max_length=255)
	location = models.CharField(max_length=255)