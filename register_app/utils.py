from register_app.models import Farmer
from annoying.functions import get_object_or_None
from register_app.models import Buyer

def registerFarmer(ussdRequest):
    phoneNumber = ussdRequest.phone_number
    session = ussdRequest.session
    name = session["name"]
    location = session["locationFarmer"]
    Farmer.objects.create(mobileNumber=phoneNumber, name=name, location=location)

def check_farmer_existence(checkExistence):
    phoneNumber = checkExistence.phone_number
    farmer = get_object_or_None(Farmer, mobileNumber=phoneNumber)
    if farmer:
        return True
    return False

def registerBuyer(buyerDetails):
    phoneNumber = buyerDetails.phone_number
    session = buyerDetails.session
    name = session["name"]
    location = session["locationBuyer"]
    Buyer.objects.create(mobileNumber=phoneNumber, name=name, location=location)

def check_buyer_existence(checkExistence):
    phoneNumber = checkExistence.phone_number
    buyer = get_object_or_None(Buyer, mobileNumber=phoneNumber)
    if buyer:
        return True
    return False